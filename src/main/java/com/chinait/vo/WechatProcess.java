package com.chinait.vo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.chinait.utils.MessageUtil;
	

/** 
 * 微信xml消息处理流程逻辑类 
 * @author saicyc 
 * 
 */  
public class WechatProcess {  
	private Logger log = Logger.getLogger(WechatProcess.class);
    /** 
     * 解析处理xml、获取智能回复结果（通过图灵机器人api接口） 
     * @param xml 接收到的微信数据 
     * @return  最终的解析结果（xml格式数据） 
     */
    public String processWechatMag(String xml){  
        /** 解析xml数据 */  
    	ReceiveXmlProcess receiveXmlProcess = new ReceiveXmlProcess();
    	ReceiveXmlEntity receiveXmlEntity = receiveXmlProcess.getMsgEntity(xml);  
        /** 以文本消息为例，调用图灵机器人api接口，获取回复内容 */  
        String result = "";  
        if("text".endsWith(receiveXmlEntity.getMsgType())){  
            result = new TulingApiProcess().getTulingResult(receiveXmlEntity.getContent());  
        }  
        /** 此时，如果用户输入的是“你好”，在经过上面的过程之后，result为“你也好”类似的内容  
         *  因为最终回复给微信的也是xml格式的数据，所有需要将其封装为文本类型返回消息 
         * */  
        //result = new FormatXmlProcess().formatXmlAnswer(receiveXmlEntity.getFromUserName(), receiveXmlEntity.getToUserName(), result);  
        return result;  
    }
    /**
     * 自动回复
     * @param xmlEntity
     * @return
     */
    public String rePly(ReceiveXmlEntity xmlEntity){
    	String respMessage = null;
		try {
			// xml请求解析
			// 发送方帐号(open_id)
			String fromUserName = xmlEntity.getFromUserName();
			// 公众帐号
			String toUserName = xmlEntity.getToUserName();
			// 消息类型
			String msgType = xmlEntity.getMsgType();
			log.info("\n==fromUserName:"+fromUserName);
			log.info("\n==toUserName:"+toUserName);
			log.info("\n==msgType:"+msgType);
			
			// 文本消息
			if (msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_TEXT)) {
				// 接收用户发送的文本消息内容
				String content = xmlEntity.getContent();
				log.info("\n=====content:"+content);
				// 创建图文消息
				NewsMessage newsMessage = new NewsMessage();
				newsMessage.setToUserName(fromUserName);
				newsMessage.setFromUserName(toUserName);
				newsMessage.setCreateTime(new Date().getTime());
				newsMessage.setMsgType(MessageUtil.RESP_MESSAGE_TYPE_NEWS);
				newsMessage.setFuncFlag(0);
				List<Article> articleList = new ArrayList<Article>();
				// 单图文消息
				if ("1".equals(content)) {
					Article article = new Article();
					article.setTitle("百合生活");
					article.setDescription("查看百合生活微信公众平台");
					article.setPicUrl("https://resource.lzbank.com:18106/cportalFileServer/files/o2o/image/pc/2016/09/22/11948/goods/20160922214030471BjOSCPc4.jpg");
					article.setUrl("http://www.bhelife.com/MallB2CWeiXin/weiXin/getWeiXinOpenId.xhtml?state=7");
					articleList.add(article);
					// 设置图文消息个数
					newsMessage.setArticleCount(articleList.size());
					// 设置图文消息包含的图文集合
					newsMessage.setArticles(articleList);
					// 将图文消息对象转换成xml字符串
					respMessage = MessageUtil.newsMessageToXml(newsMessage);
				}
			}else if(msgType.equals(MessageUtil.REQ_MESSAGE_TYPE_EVENT)){
				// 接收用户发送的事件请求内容
				String Event = xmlEntity.getEvent();
				String EventKey = xmlEntity.getEventKey();
			}
		} catch (Exception e) {
			log.error("自动回复",e);
		}
		return respMessage;
    }
    public static String emoji(int hexEmoji) {
		return String.valueOf(Character.toChars(hexEmoji));
	}
}  