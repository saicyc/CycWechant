package com.chinait.vo;

public class FileResultVO {
	private String file;
	private String fileName;
	private String fileThumbnail;
	public String getFile() {
		return file;
	}
	public void setFile(String file) {
		this.file = file;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileThumbnail() {
		return fileThumbnail;
	}
	public void setFileThumbnail(String fileThumbnail) {
		this.fileThumbnail = fileThumbnail;
	}
}
