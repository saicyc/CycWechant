package com.chinait.vo;

public enum WeiXinMenuEnum {
	/**1：微信购物*/
	WEIXIN_WEIXINGOUWU("1","http://www.h5ds.com"),
	/**2：全球购*/
	WEIXIN_QUANQIUGOU("2","https://kj.lzbank.com/MallOverseaWeb/goodshtml/index5.html"),
	/**3：牛肉面*/
	WEIXIN_NIUROUMIAN("3","http://oneit.vicp.io/MallMobile/staticHtml/beefNodlesActivity/noodles_index.html"),
	/**4：限时抢购*/
	WEIXIN_XIANSHIQIANGGOU("4","http://etest.lzbank.com:20009/MallMobile/html/wechat/activity/flashSale.html"),
	/**5：特价秒杀*/
	WEIXIN_TEJIAMIAOSHA("5","http://etest.lzbank.com:20009/MallMobile/html/wechat/activity/panicBuying.html"),
	/**6：新品首发*/
	WEIXIN_XINPINSHOUFA("6","http://etest.lzbank.com:20009/MallMobile/html/wechat/activity/newProduct.html"),
	/**7：大转盘*/
	WEIXIN_DAZHUANPAN("7","http://etest.lzbank.com:20009/MallMobile/html/wechat/activity/turntable.html"),
	/**8：在线客服*/
	WEIXIN_ZAIXIANKEFU("8",""),
	/**9：个人中心*/
	WEIXIN_GERENZHONGXIN("9","http://etest.lzbank.com:20009/MallMobile/html/wechat/main/personalIndex.html"),
	/**10：跨境电商商品详情*/
	WEIXIN_KJDS_GOODSINFO("10","http://etest.lzbank.com:20009/MallMobile/weiXinGoods/wechat//forwardOverseaAppGoods.xhtml");
	
	private String code; //订单状态代码
	private String desc; //订单状态描述
	
	
	WeiXinMenuEnum(String code,String desc){
		this.code=code;
		this.desc=desc;
	}

	public static String getDesc(String code){
		for(WeiXinMenuEnum s: WeiXinMenuEnum.values()){
			if(s.getCode().equals(code)){
				return s.desc;
			}
		}
		return null;
	}
	
	
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}


	public String getDesc() {
		return desc;
	}


	public void setDesc(String desc) {
		this.desc = desc;
	}
}
