package com.chinait.dao;

import org.springframework.data.repository.CrudRepository;

import com.chinait.domain.AppUser;

public interface AppUserRepository extends CrudRepository<AppUser, Integer>{

	AppUser findByLoginNameAndIsDeleteIsFalse(String userName);

}
