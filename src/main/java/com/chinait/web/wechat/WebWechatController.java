package com.chinait.web.wechat;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.RequestMapping;

import com.chinait.config.WeChatProperties;
import com.chinait.utils.HttpSendUtils;
import com.chinait.vo.WechatUserInfoVo;
import com.google.gson.Gson;
/**
 * 网页服务(按照服务的范围来进行划分)
 * @author cyc
 */
@Controller
@RequestMapping("webWechat")
public class WebWechatController {
	private static final Logger logger = LoggerFactory.getLogger(WebWechatController.class);
	@Autowired
	WeChatProperties weChatProperties;
	/**
	 * 网页的授权接口
	 * @param state
	 * @return
	 */
	@RequestMapping("fromWechat")
	public String fromWechat(String state){
		//传入的参数 （这里主要用于网页授权回调地址的判断）
		if(StringUtils.isEmpty(state)){
			return "/error";
		}
		//通过微信的oauth 拿到用户的code 值
		String authUrl = weChatProperties.getAuthUrl();
		String redirectUrl = weChatProperties.getAuthReturnUrl();
		logger.info("\n=============authUrl :"+authUrl);
		logger.info("\n=============redirectUrl :"+redirectUrl);
		/**
		 * scope 的状态有两种
		 * snsapi_base : 基本的用户信息权限认证，只针对关注了该微信的服务号
		 * snsapi_userinfo：基本的用户信息权限认证，如果没有关注该微
		 * 信的服务号的用户会弹出一个绿色的框并提示确认授权，
		 * 关注了就不会有这个了。（注意这里如果没有关注微信公众号，但是网页授权了之后，会生成一个零时的微信的openID）
		 * https://open.weixin.qq.com/connect/oauth2/authorize?appid=APPID&redirect_uri=REDIRECT_URI&response_type=code&scope=SCOPE&state=STATE#wechat_redirect
		 */
		return authUrl+weChatProperties.getAppId()+"&redirect_uri="+redirectUrl+"&response_type=code&scope=snsapi_userinfo&state="+state+"#wechat_redirect";
	}
	/**
	 * 网页授权回调的地址
	 * @param state
	 * @param code
	 * @return
	 */
	@RequestMapping("authReturnUrl")
	public String authReturnUrl(String state,String code){
		if(StringUtils.isEmpty(state)||StringUtils.isEmpty(state)){
			return "error";
		}
		//获取微信的网页的accessT
		//state(传入的参数 （这里主要用于网页授权回调地址的判断）)
		//code 值用于获取openId 的详细信息
		//https://api.weixin.qq.com/sns/oauth2/access_token?appid=APPID&secret=SECRET&code=CODE&grant_type=authorization_code
		String accessTokenUrl = weChatProperties.getGetPageAccessToken()+weChatProperties.getAppId()+"&secret="+weChatProperties.getAppSecret()+"&code="+code+"&grant_type=authorization_code";
		String returnDatas = HttpSendUtils.sendGet(accessTokenUrl);
		Gson gson = new Gson();
		//这里的接口只能获取到了一般接口网页调用的accessToken 和 用户openId
		WechatUserInfoVo wechatUserInfoVo = gson.fromJson(returnDatas, WechatUserInfoVo.class);
		/**
		 * 网页的access_Token
		 * JfCWVK3Ctenz_VkDUkrmhlO40ROyaDeOuTp92t80XnHsQfCR939crCjWrRnBXO4PHC6EtU87lsfAoZtkqs8S8_0DSFCZLzq9rSP5P43Fo
		 * 接口用的token 
		 * Z2ZsmdXgbFTJ0pFYJ86YHkn8xos2hG6mFcmp6OG8co9rhm3HfRcoXxR_KcAx8qfCMMpNO_m02JU2QEw16v7BZnvg4CjTzFR1KCdh85tVjRIEI0ZNvJqZpuh2uOQ1VU09ARDfADASJV
		 * 可以看出这两个token 的长度不是一样的这里可以确定不是同一个东西
		 */
		String openId  = wechatUserInfoVo.getOpenid();
		logger.info("\n=================openId:"+openId);
		//获取接口传输的accessToken 需要调用这个接口
		String tokenUrl = weChatProperties.getToken()+weChatProperties.getAppId()+"&secret="+weChatProperties.getAppSecret();
		String returnData=HttpSendUtils.sendGet(tokenUrl);
		wechatUserInfoVo = gson.fromJson(returnData, WechatUserInfoVo.class);
		//这里获取了accessToken 这个是接口调用的
		logger.info("\n=================accessToken:"+wechatUserInfoVo.getAccess_token());
		//获取 ticket 通过accessToken 来进行获取
		String jsapiUrl = weChatProperties.getJsapi()+wechatUserInfoVo.getAccess_token()+"&type=jsapi";  
		String backDatas=HttpSendUtils.sendGet(jsapiUrl);
		//gson.fromJson(backDatas, (new TypeToken<Map<String,Object>>(){}).getClass());
		return "";
	}
}
