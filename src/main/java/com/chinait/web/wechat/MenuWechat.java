package com.chinait.web.wechat;


import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.HttpMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import com.chinait.utils.HttpSendUtils;
import com.chinait.vo.AccessTokenVo;
import com.chinait.vo.ViewButton;
import com.chinait.vo.WeiXinMenuEnum;
import com.google.gson.Gson;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;


public class MenuWechat {
	private static final Logger logger = LoggerFactory.getLogger(MenuWechat.class);
	private static String weixin_openId_url = "";
	private static String weixin_appId = "";
	private static String weixin_appsecret = "";
	private static String weixin_GrantType = "";
	private static String weixin_create_menu = "";
	private static String weixin_accessToken = "";
	public static void main(String[] args) {
		
		ViewButton vbt2=new ViewButton();
	    vbt2.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_QUANQIUGOU.getCode());
	    vbt2.setName("全球购");
	    vbt2.setType("view");
	    ViewButton vbt3=new ViewButton();
	    vbt3.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_NIUROUMIAN.getCode());
	    vbt3.setName("牛肉面");
	    vbt3.setType("view");
	    JSONArray subButton=new JSONArray();
	    //subButton.add(vbt1);
	    subButton.add(vbt2);
	    subButton.add(vbt3);
	    ViewButton vbt4=new ViewButton();
	    vbt4.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_XIANSHIQIANGGOU.getCode());
	    vbt4.setName("限时抢购");
	    vbt4.setType("view");
	    
	    ViewButton vbt5=new ViewButton();
	    vbt5.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_TEJIAMIAOSHA.getCode());
	    vbt5.setName("整点秒杀");
	    vbt5.setType("view");
	    
	  /*  ViewButton vbt6=new ViewButton();
	    vbt6.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_XINPINSHOUFA.getCode());
	    vbt6.setName("新品首发");
	    vbt6.setType("view");*/
	    
	    ViewButton vbt7=new ViewButton();
	    vbt7.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_DAZHUANPAN.getCode());
	    vbt7.setName("大转盘");
	    vbt7.setType("view");

	    JSONArray subButtonTwo=new JSONArray();
	    subButtonTwo.add(vbt4);
	    subButtonTwo.add(vbt5);
	    //subButtonTwo.add(vbt6);
	    subButtonTwo.add(vbt7);
	    
	    /*ViewButton vbt8=new ViewButton();
	    vbt8.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_ZAIXIANKEFU.getCode());
	    vbt8.setName("在线客服");
	    vbt8.setType("view");*/
	    
	    ViewButton vbt9=new ViewButton();
	    vbt9.setUrl(weixin_openId_url+WeiXinMenuEnum.WEIXIN_GERENZHONGXIN.getCode());
	    vbt9.setName("个人中心");
	    vbt9.setType("view");
	    
	    JSONArray subButtonThree=new JSONArray();
	   /* subButtonThree.add(vbt8);*/
	    subButtonThree.add(vbt9);
	    
	    JSONObject buttonOne=new JSONObject();
	    buttonOne.put("name", "百合购物");
	    buttonOne.put("sub_button", subButton);
	    
	    JSONObject buttonTwo=new JSONObject();
	    buttonTwo.put("name", "粉丝福利");
	    buttonTwo.put("sub_button", subButtonTwo);
	    
	    JSONObject buttonThree=new JSONObject();
	    buttonThree.put("name", "我的服务");
	    buttonThree.put("sub_button", subButtonThree);
	    
	    JSONArray button=new JSONArray();
	    button.add(buttonOne);
	    button.add(buttonTwo);
	    button.add(buttonThree);
	    
	    JSONObject menujson=new JSONObject();
	    menujson.put("button", button);
	    String access_token = getAccessToken(weixin_appId,weixin_appsecret,weixin_GrantType);//getAccessToken(appId,appSecret,grantType);
	    String menuUrl = null;
	    if(StringUtils.isEmpty(access_token)){
	    	//这里为请求接口的url   +号后面的是token，这里就不做过多对token获取的方法解释
	    	menuUrl = weixin_create_menu+access_token;
	    }
	    try{
	      //String s = "{\"button\":[{\"name\":\"cyc\",\"sub_button\":[{\"type\":\"click\",\"name\":\"笑话大全\",\"key\":\"m_joke\"},{\"type\":\"click\",\"name\":\"内涵段子\",\"key\":\"m_duanzi\"},{\"type\":\"click\",\"name\":\"爆笑图片\",\"key\":\"m_laughImg\"}]},{\"name\":\"实用工具\",\"sub_button\":[{\"type\":\"click\",\"name\":\"天气查询\",\"key\":\"m_weather\"},{\"type\":\"click\",\"name\":\"公交查询\",\"key\":\"m_bus\"},{\"type\":\"click\",\"name\":\"功能菜单\",\"key\":\"m_sysmenu\"}]},{\"name\":\"消息示例\",\"sub_button\":[{\"type\":\"click\",\"name\":\"关于企特\",\"key\":\"m_about\"},{\"type\":\"click\",\"name\":\"图文消息\",\"key\":\"m_imgmsg\"},{\"type\":\"click\",\"name\":\"音乐消息\",\"key\":\"m_musicmsg\"}]}]}";
	    	HttpClient httpClients = new HttpClient();
	    	HttpMethod method=new PostMethod(weixin_create_menu);
	    	httpClients.executeMethod(method);
	    	//String code = method.
	    	String rs = method.getResponseBodyAsString();
	        logger.info("rs:============="+rs);
	    }catch(Exception e){
	    	logger.error("设置菜单报错！==", e);
	    }
	}
	 public static String contactUrl(String startUrl, String redrectUrl, String endUrl) throws UnsupportedEncodingException{
		return startUrl+URLEncoder.encode(redrectUrl,"UTF-8")+endUrl;
	  }
	 /**
	   * 微信获取accessToken
	   * @param appId
	   * @param appSecret
	   * @return
	   */
	  public static String getAccessToken(String appId, String appSecret,String grantType){
		String url = weixin_accessToken+grantType+"&appid="+appId+"&secret="+appSecret;
		String rs = HttpSendUtils.sendGet(url);
		AccessTokenVo map = null;
		if(!StringUtils.isEmpty(rs)){
			Gson gson = new Gson();
			map = gson.fromJson(rs, AccessTokenVo.class);
		}
		String access_token = map.getAccess_token();
		return access_token;
	  }
}