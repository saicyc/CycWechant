package com.chinait.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties(prefix = "wechat")
public class WeChatProperties {
	private String appSecret;
	private String appId;
	private String authUrl;
	private String authReturnUrl;
	private String getPageAccessToken;
	private String token;
	private String jsapi;
	
	public String getAppSecret() {
		return appSecret;
	}
	public void setAppSecret(String appSecret) {
		this.appSecret = appSecret;
	}
	public String getAppId() {
		return appId;
	}
	public void setAppId(String appId) {
		this.appId = appId;
	}
	public String getAuthUrl() {
		return authUrl;
	}
	public void setAuthUrl(String authUrl) {
		this.authUrl = authUrl;
	}
	public String getAuthReturnUrl() {
		return authReturnUrl;
	}
	public void setAuthReturnUrl(String authReturnUrl) {
		this.authReturnUrl = authReturnUrl;
	}
	public String getGetPageAccessToken() {
		return getPageAccessToken;
	}
	public void setGetPageAccessToken(String getPageAccessToken) {
		this.getPageAccessToken = getPageAccessToken;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
	public String getJsapi() {
		return jsapi;
	}
	public void setJsapi(String jsapi) {
		this.jsapi = jsapi;
	}
	
}
