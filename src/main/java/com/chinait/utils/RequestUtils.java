package com.chinait.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;

import javax.servlet.http.HttpServletRequest;
/**
 * request 的工具类
 * @author cyc
 *
 */
public class RequestUtils {
	/**
	 * request 的Stream 转 Xml
	 * @param request
	 * @return
	 * @throws IOException 
	 */
	public static String streamToXml(HttpServletRequest request) throws IOException {
		/** 读取接收到的xml消息 */  
        StringBuffer sb = new StringBuffer();  
        InputStream is = request.getInputStream();  
        InputStreamReader isr = new InputStreamReader(is, "UTF-8");  
        BufferedReader br = new BufferedReader(isr);  
        String s = "";  
        while ((s = br.readLine()) != null) {  
            sb.append(s);  
        }  
		return sb.toString(); //次即为接收到微信端发送过来的xml数据  
	}

}
